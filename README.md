## Fullstack - Developer 

Parabéns, você passou para a segunda fase do processo seletivo da NEXTI para desenvolvedor Fullstack (Java + Angular).

## Instruções

- Disponibilizar o repositório público no Github e documentar a execução no README. Enviar o link talentos@nexti.com

### Funcionalidades

- Desenvolver uma aplicação web responsável por gerenciar marcações de ponto.
- *O investidor solicita um sistema onde possa cadastrar os seus clientes, funcionários e suas marcações de ponto.* 
Desta forma o mesmo precisa vincular funcionários a cliente e visualizar as marcações dos funcionários exibindo as horas extras e faltas.
- *O analista de negócio reuniu mais informações e a seguinte demanda foi gerada para ser executada que está descrita logo abaixo.*


### Demanda
- *Deve ser seguida conforme solicitado.*

- Cliente
	- Incluir, excluir, atualizar e listar.
- Funcionário
	- Incluir, excluir, atualizar e listar.
    - Vincular funcionários a um cliente.
    - Atribuir uma escala semanal, com a marcação esperada de entrada e saída.
- Painel para exibir pontos do funcionário por dia, contendo horas falta ou horas extras, 
    e caso exista alguma falta mostras a ausência e a sua justificativa.


**Cliente** 

O cliente deve ter os seguintes atributos:
```
* Razão social
* Nome fantasia
* CNPJ
```

De forma que sua tabela `client` contenha as seguintes colunas:
```
* `id INT NOT NULL AUTOINCREMENTE`
* `company_name VARCHAR(120) NOT NULL`
* `fantasy name VARCHAR(120) NOT NULL`
```

**Funcionário** 

O funcionário deve ter os seguintes atributos:
```
* Nome
* Matrícula
* CPF
* Data de nascimento
* Email [UNIQUE]
* Data de admissão
```

De forma que sua tabela `people` contenha as seguintes colunas:
```
* `id INT NOT NULL AUTOINCREMENTE`
* `name VARCHAR(255) NOT NULL`
* `enrolment VARCHAR(12) NOT NULL`
* `cpf VARCHAR(11) NOT NULL`
* `date_of_birth DATE NULL`
* `email VARCHAR(255) UNIQUE NOT NULL`
* `admission_date DATETIME NULL`
```

O vinculo de cliente e funcionário, deve ser armazenados em uma tabela relacional `client_people` contendo as seguintes colunas:
```
* `client_id` INT
* `people_id` INT
* `start_date DATE`
```

As marcações de entrada e saída por dia que definem a escala do funcionário, devem ser armazenados numa tabela relacional `people_times` contendo as seguintes colunas:
```
* `id` INT
* `people_id` INT
* `weekday INT` (0=>Domingo, 6=>Sábado)
* `marking_in INT`
* `marking_out INT`
```

Esta tabela deve conter um unique key `UNIQUE(people_id, weekday)` para impedir entradas duplicadas.

**Ponto**

Funcionário pode realizar as marcações de  ponto na entrada ou de saída e já exibir as estatísticas de horas faltantes ou extras. 

As "batidas de ponto" devem ser armazenadas na tabela relacional `time_register`:
```
* `id`
* `people_id`
* `date DATE`
* `marking_in INT NULL`
* `marking_out INT NULL`
* `justification VARCHAR(255) NULL`
```

Esta tabela deve conter um unique key `UNIQUE(people_id, date)` para impedir entradas duplicadas.
As colunas `marking_in` e `marking_out` podem ser nulas pois esta mesma tabela servirá para justificar faltas, atrasos e saídas antecipadas.


### Desafio
- O desenvolvimento do backend deve ser feito em Java.
- Boa documentação de código e de serviços.
- Desenvolver os módulos de frontend e backend de forma separada.
- O desenvolvimento do frontend pode utilizar JavaScript e qualquer framework ou ferramenta que suportem ou utilizem estas tecnologias.
- Preferencialmente utilizar Spring Boot 1.4+ com toda sua stack para o desenvolvimento do backend.
- Preferencialmente utilizar AngujarJS para o desenvolvimento do frontend.(Diferencial ReactJS, Angular 2+)
- Preferencialmente utilizar Mysql o desenvolvimento do backend.(Aceitável PostgreSQL)
- Não é necessário submeter uma aplicação que cumpra cada um dos requisitos descritos, mas o que for submetido deve funcionar.
- Boas práticas de UX na solução.
- Testes do código.

### Avaliação
- Será visto no código desenvolvido: clean code; resultado funcional; entre outros fatores, e manutenção. 
- Documente todo o processo necessário para executarmos o seu projeto, para isso esperamos que utilize o README no git.
- Explique as decisões técnicas tomadas caso necessário, as escolhas por bibliotecas e ferramentas, o uso de patterns etc.


